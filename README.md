# Meetup Calendar Kiosk Setup

To get wifi working on a Raspberry Pi running NOOBS, try:

* https://kerneldriver.wordpress.com/2012/10/21/configuring-wpa2-using-wpa_supplicant-on-the-raspberry-pi/

Set up Chromium with the following plugins:

* Tampermonkey (for running userscripts)
  * With the kioskify-meetup-events userscript from this folder or: https://greasyfork.org/en/scripts/376649-kioskify-meetup-com-events

Set Chromium to start on boot:

* Edit Startup Apps
